
import {Fragment} from 'react'

import Highlights from '../components/Highlights';
// import AppNavbar from './../components/AppNavbar';
import Banner from './../components/Banner';
// import Footer from './../components/Footer';


export default function Home(){

    const data = {
        title: "Welcome to Course Booking",
        description: "Opportunities for everyone, everywhere",
        destination: "/courses",
        buttonDesc: "Check Courses"
    }

    return(
        <Fragment>
            {/* <AppNavbar/> */}
            <Banner bannerProp={data}/>
            <Highlights/>

            {/* <Footer/> */}
        </Fragment>
    )
}