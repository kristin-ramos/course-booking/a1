
import {Row, Col, Button, Form} from 'react-bootstrap'
import {useEffect, useState} from 'react'
import { Navigate, useNavigate } from 'react-router-dom'

export default function Login(){

    const [email, setEmail] = useState("")
    const [pw, setPw] = useState("")
    const [isDisabled, setIsDisabled] = useState(true)
    
    const navigate = useNavigate()

    useEffect(()=>{
        if(email !== "" && pw !== ""){
            setIsDisabled(false)
        }else{
            setIsDisabled(true)
        }
    }, [email, pw])

    const loginUser = async (e)=>{
        e.preventDefault()

        await fetch('http://localhost:3007/api/users/login', {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: email,
                password: pw
            })
        })
        .then(response=>response.json())
        .then(response=>{
            console.log(response)
            
            if(response){
                localStorage.setItem('token', response.token)
                localStorage.setItem('email', email)

                // console.log(localStorage.getItem(email))
                alert(`login succesful`)
                setEmail("")
                setPw("")
                // console.log("Email:", email)
                // console.log("PW:", pw)

                navigate('/courses')

            }else{
                alert(`user does not exist`)
            }
        })
     }


    return(
        <Row className="m-5">
            <Col>
                <h3 className='text-center'>Login</h3>
                <Form onSubmit={(e)=> loginUser(e)}>
                    <Form.Group className="mb-3" >
                        <Form.Label>Email address</Form.Label>
                        <Form.Control type="email" value={email} onChange={(e)=>{setEmail(e.target.value)}}/>
                    </Form.Group>
                
                    <Form.Group className="mb-3" >
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" value={pw}onChange={(e)=>{setPw(e.target.value)}}/>
                    </Form.Group>
                                
                    <Button variant="info" type="submit" disabled={isDisabled} >
                    Submit
                    </Button>
                </Form>
            </Col>
        </Row>
    )
}