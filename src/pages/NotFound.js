
import { Fragment } from "react";
import Banner from "../components/Banner";

export default function NotFound (){

    const data = {
        title: "Error 404",
        description: "Page not found",
        destination: "/",
        buttonDesc: "Go back home"
    }

    return (
        <Fragment>
            <Banner bannerProp={data}/>
        </Fragment>
    )
}