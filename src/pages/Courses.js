
import { Fragment, useContext } from 'react';
import CourseCard from '../components/CourseCard';
import coursesData from '../mockData/courses';
import UserContext from '../UserContext';



export default function Courses(){
    // console.log(coursesData[1])

    const {user} = useContext(UserContext)
    console.log(user)

    const courses=coursesData.map(course=>{
        // console.log(course)
        return <CourseCard key={course.id} courseProp={course} />
    })

    return(
        // {courses}
        // <h1>hello</h1>
        <Fragment>
            {courses}
        </Fragment>
    )
}