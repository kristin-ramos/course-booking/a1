import {Form, Button, Row, Col, Container, FormControl} from 'react-bootstrap'
import {useState, useEffect} from 'react'
import { useNavigate } from 'react-router-dom'

export default function Register(){

    const [fN, setFn] = useState("")
    const [lN, setLn] = useState("")
    const [email, setEmail] = useState("")
    const [pw, setPw] = useState("")
    const [vpw, setVpw] = useState("")
    const [isDisabled, setIsDisabled] = useState(true)
    const navigate = useNavigate();

    // useEffect(function, options)
    useEffect(()=>{
        console.log('render')
        // if all input fields are empty,keep the state of the buttons to true
        // else if all fields are filled out and pw & vpw are equal, change the stateto false
        // listen to state changes: fn, ln, em, pw, vp

        if((fN !== "" && lN !== "" && email !== "" && pw !== "" && vpw !== "") && (pw == vpw)){
            setIsDisabled(false)
        }else{
            setIsDisabled(true)
        }
    }, [fN, lN, email, pw, vpw])

    const registerUser = (e)=>{
        e.preventDefault()
        // console.log(e)

        fetch('http://localhost:3007/api/users/email-exists', {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: email
            })
        }).then(response=> response.json())
        .then(response=>{
            // console.log(response)
            if(!response){
                // send request to register
                fetch('http://localhost:3007/api/users/register', {
                method: "POST",
                headers: {
                "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    firstName: fN,
                    lastName: lN,
                    email: email,
                    password: pw,
            })
        })
        .then(response=> response.json())
        .then(response=>{
            if(response){
                alert(`registration successful.`)
                navigate('/login')
            }else{
                alert('something went wrong')
            }
        })
            }else{
                alert(`user already exists`)
            }
        })
    }



    return(
    <Container className="m-5">
        <h3 className="text-center">Register</h3>
        <Row className="justify-content-center">
            <Col xs={12} md={6}>
                <Form onSubmit={(e)=> registerUser(e)}>

                    <Form.Group className="mb-3">
                        <Form.Label>First Name</Form.Label>
                        <Form.Control 
                        type="text" 
                        value={fN} 
                        onChange={(e)=>{setFn(e.target.value)}}
                        />
                    </Form.Group>
                    
                    <Form.Group className="mb-3">
                        <Form.Label>Last Name</Form.Label>
                        <Form.Control type="text" value={lN}
                        onChange={(e)=>{setLn(e.target.value)}}/>
                    </Form.Group>

                    <Form.Group className="mb-3">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control type="email" value={email}
                        onChange={(e)=>{setEmail(e.target.value)}}/>
                    </Form.Group>
                    
                    <Form.Group className="mb-3">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" value={pw}
                        onChange={(e)=>{setPw(e.target.value)}}/>
                    </Form.Group>

                    <Form.Group className="mb-3">
                        <Form.Label>Verify Password</Form.Label>
                        <Form.Control type="password" value={vpw} onChange={(e)=>{setVpw(e.target.value)}}/>
                    </Form.Group>

                    <Button variant="info" type="submit" disabled={isDisabled} >
                    Submit
                    </Button>
                </Form>
            
            </Col>
        </Row>
    </Container>
    )
}