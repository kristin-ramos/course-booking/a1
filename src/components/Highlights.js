import { Fragment } from "react";
import { Row, Col, Card } from "react-bootstrap";

export default function Highlights (){
    return(
        <Fragment>
        <Row className="m-5">
            <Col xs={12} md={4}>
                    <Card>
                    <Card.Body>
                        <Card.Title>Learn From Home</Card.Title>
                        <Card.Text>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos, soluta iste? Explicabo, tempore aliquid. Sit, optio adipisci magnam eligendi ea nobis ducimus pariatur natus voluptates repellendus! Vitae exercitationem modi accusamus.

                        </Card.Text>
                        
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
            <Card>
                    <Card.Body>
                        <Card.Title>Study now Pay later</Card.Title>
                        <Card.Text>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos, soluta iste? Explicabo, tempore aliquid. Sit, optio adipisci magnam eligendi ea nobis ducimus pariatur natus voluptates repellendus! Vitae exercitationem modi accusamus.

                        </Card.Text>
                        
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
            <Card>
                    <Card.Body>
                        <Card.Title>Be Part of our Community</Card.Title>
                        <Card.Text>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos, soluta iste? Explicabo, tempore aliquid. Sit, optio adipisci magnam eligendi ea nobis ducimus pariatur natus voluptates repellendus! Vitae exercitationem modi accusamus.

                        </Card.Text>
                        
                    </Card.Body>
                </Card>
            </Col>
        </Row>
        </Fragment>
    )
}