

import { Fragment } from "react"
import Home from "../pages/Home"
import { Route } from "react-router-dom"


export default function Banner({bannerProp}) {
    // console.log(bannerProp)

    const {title, description, destination, buttonDesc} = bannerProp
    
    return (

        <div className="jumbotron jumbotron-fluid">
            <div className="container">
                   <h1 className="display-4">{title}</h1>
                   <p className="lead">{description}</p>
                   <a className="btn btn-info" href={destination}>{buttonDesc}</a>
            </div>
        </div>
    )
}