import {useState, useEffect} from 'react'
import {Card, Button} from 'react-bootstrap'

export default function CourseCard ({courseProp}) {

    // console.log(props)
    console.log(courseProp)

    let [count, setCount] = useState(0)

    let [seat, setSeat] = useState(30)

    const {name, description, price} = courseProp
    // console.log(name)
    // console.log(description)
    // console.log(price)
    
    useEffect(()=>{
        console.log('render')
    }, [count])


    const handleClick = () => {
        // console.log(`I'm clicked`, count + 1)
        // setCount(count+1)

        if(seat!= 0){
            for(let i=30; i>0; i--){
                setSeat(seat-1)
                setCount(count+1)
            }
        }else{
            if(seat==0){ 
                return alert(`no more seats`)
            }
        }
    }

    

    return (
        <Card className = "m-5">
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description</Card.Subtitle>
                <Card.Text>
                {description}
                </Card.Text>
                <Card.Text>{price}
                </Card.Text>
                <Card.Text>Count: {count}</Card.Text>
                <Card.Text>Enrollees: {count} Enrollees</Card.Text>
                <Button className="btn-info" onClick={handleClick}>Enroll</Button>
            </Card.Body>
        </Card>
    
    )
}