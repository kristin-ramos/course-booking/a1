
let coursesData = [
    {
        id: "wdc001",
        name: "PHP-Laravel",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos, soluta iste? Explicabo, tempore aliquid. Sit, optio adipisci magnam eligendi ea nobis ducimus pariatur natus voluptates repellendus! Vitae exercitationem modi accusamus.",
        price: 25000,
        onOffer: true
    },
    {        
        id: "wdc002",
        name: "Python-Dyango",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos, soluta iste? Explicabo, tempore aliquid. Sit, optio adipisci magnam eligendi ea nobis ducimus pariatur natus voluptates repellendus! Vitae exercitationem modi accusamus.",
        price: 25000,
        onOffer: true
    },
    {
        id: "wdc003",
        name: "Java-Springboot",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos, soluta iste? Explicabo, tempore aliquid. Sit, optio adipisci magnam eligendi ea nobis ducimus pariatur natus voluptates repellendus! Vitae exercitationem modi accusamus.",
        price: 45000,
        onOffer: true,
    },
    {
        id: "wdc004",
        name: "NodeJS-ExpressJS",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos, soluta iste? Explicabo, tempore aliquid. Sit, optio adipisci magnam eligendi ea nobis ducimus pariatur natus voluptates repellendus! Vitae exercitationem modi accusamus.",
        price: 55000,
        onOffer: false,
    }

]

export default coursesData