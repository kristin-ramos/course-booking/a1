import React, {Fragment} from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';

import App from './App';

// RactDOM.render(<App/>, document.getElementById('root'));

ReactDOM.render(
  <Fragment>
    <App />

  </Fragment>, document.getElementById('root'));


